//
//  AppDelegate.h
//  GitlabTest
//
//  Created by Cattlefield on 19/12/18.
//  Copyright © 2018 Cattlefield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

