//
//  main.m
//  GitlabTest
//
//  Created by Cattlefield on 19/12/18.
//  Copyright © 2018 Cattlefield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
